package k�che;

import java.awt.Color;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringEscapeUtils;

public class SimulationNeu {

	// public EventList futureEventList=new EventList();
	private SortedMap<Double, Bestellung> futureEventList = new TreeMap<>();
	public double simzeit;
	public double endzeit;
	public char koch1;
	public char koch2;
	public double b1;
	public double b2;
	public double ankunftEreignis;
	public double endereignis;
	public int warteschlange = 0;
	public Random rand = new Random();
	public double zufriedenKoch1;
	public double zufriedenKoch2;
	public boolean flag_zufriedenheit_1;
	public boolean flag_zufriedenheit_2;
	public String StringZufriedenK1;
	public String StringZufriedenK2;
	public int count;
	public int erwartungAnk; 
	public int erwartungBea;
	public Queue<Bestellung> queue = new LinkedList<Bestellung>();

	public void initialisierung(JTable table, double simEnde, BufferedWriter schreibBuffer, int erwartungAnk, int erwartungBea) throws IOException {
		koch1 = 'f';
		koch2 = 'f';
		simzeit = 0.0;
		this.erwartungAnk = erwartungAnk;
		this.erwartungBea = erwartungBea;
		count=0;
		endzeit = simEnde;
		flag_zufriedenheit_1=true;
		flag_zufriedenheit_2=true;
		ankunftEreignis = zufallGeneratorAnkunft(erwartungAnk);
		Bestellung evt = new Bestellung(ankunftEreignis, 1);
		futureEventList.put(ankunftEreignis, evt);

		ausgabeGUI(table);
		csvDatei(schreibBuffer);
		
	}

	public void Simulation(JTable table, double simende, int erwartungAnk, int erwartungBea) {

		try {
			String verzeichnis = System.getProperty("user.home");
			Path pfadMitDatei = Paths.get(verzeichnis, "daten.xls");
			BufferedWriter schreibBuffer = Files.newBufferedWriter(pfadMitDatei);
			initialisierung(table, simende, schreibBuffer,erwartungAnk,erwartungBea);

			while (simzeit < endzeit) {

				Bestellung best = (Bestellung) futureEventList.get((futureEventList.firstKey()));
				simzeit = best.getZeit();
				int typ = best.getType();

				if (typ == 1) { // Ankunftsereignis
					ankunftEreignis();
				}
				if (typ == 2) { // Endereignis koch1
					endEreignis(2);
				}
				if (typ == 3) { // Endereignis koch2
					endEreignis(3);
				}

				ausgabeGUI(table);
				csvDatei(schreibBuffer);
				
			}
			csvFormeln(schreibBuffer);
			schreibBuffer.flush();
			schreibBuffer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void ankunftEreignis() {

		ankunftEreignis = zufallGeneratorAnkunft(erwartungAnk);
 		ankunftEreignis = ankunftEreignis + simzeit;
		Bestellung evt = new Bestellung(ankunftEreignis, 1);
		futureEventList.put(ankunftEreignis, evt);
		
		if (koch1 == 'f') {
			koch1 = 'a';
			Bestellung best = futureEventList.get(futureEventList.firstKey());
			double zeit = best.getZeit();
			futureEventList.remove((futureEventList.firstKey())); // Ankunftsereignis
																	// l�schen
			endereignis = zufallGeneratorEnde(erwartungBea);
			endereignis = endereignis + simzeit;
			b1 = endereignis;
			evt = new Bestellung(endereignis, zeit, 2);
			futureEventList.put(endereignis, evt);
		} else if (koch2 == 'f') {
			koch2 = 'a';
			Bestellung best = futureEventList.get(futureEventList.firstKey());
			double zeit = best.getZeit();
			futureEventList.remove((futureEventList.firstKey())); // Ankunftsereignis
																	// l�schen
			endereignis = zufallGeneratorEnde(erwartungBea);
			endereignis = endereignis + simzeit;
			b2 = endereignis;
			evt = new Bestellung(endereignis, zeit, 3);
			futureEventList.put(endereignis, evt);
		} else {
			queue.add(futureEventList.get(futureEventList.firstKey()));
			warteschlange++;
			futureEventList.remove((futureEventList.firstKey())); // Ankunftsereignis
			

		}

	}

	public void endEreignis(int bedinungsendeTyp) {
		double zeitGekommen=0;
		double zeitGegangen=0;
		Bestellung best;
		if(warteschlange > 0){
			 best = queue.poll();
			 zeitGekommen=best.getZeit2();
			 zeitGegangen = best.getZeit();
		}else{
			 best = futureEventList.get(futureEventList.firstKey());
			 zeitGekommen = best.getZeit2();
			 zeitGegangen = best.getZeit();	
			futureEventList.remove((futureEventList.firstKey()));
		}
		
		
		if (bedinungsendeTyp == 2) {
			if (warteschlange > 0) {
				warteschlange--; // entferne kunde aus Warteschlange

				// Plane neues Endeereignis
				endereignis = zufallGeneratorEnde(erwartungBea);
				endereignis = endereignis + simzeit;
				Bestellung evt = new Bestellung(endereignis, zeitGekommen, 2);
				futureEventList.put(endereignis, evt);
				b1 = endereignis;
			} else {
				koch1 = 'f';
				b1 = 0.0;
				zufriedenKoch1 = zeitGegangen - zeitGekommen;
				flag_zufriedenheit_1=true;
				if(zufriedenKoch2<=15.00){
					StringZufriedenK1="\u2713";
					}else{
					StringZufriedenK1="\u2716"; 
					}
			}
		}
		if (bedinungsendeTyp == 3) {
			if (warteschlange > 0) {
				warteschlange--;// entferne kunde aus Warteschlange

				// Plane neues Endeereignis
				endereignis = zufallGeneratorEnde(erwartungBea);
				endereignis = endereignis + simzeit;
				Bestellung evt = new Bestellung(endereignis, zeitGekommen, 3);
				futureEventList.put(endereignis, evt);
				b2 = endereignis;
			} else {
				koch2 = 'f';
				b2 = 0.0;
				flag_zufriedenheit_2=true;
				zufriedenKoch2 = zeitGegangen - zeitGekommen;
				
				if(zufriedenKoch2<=15.00){
				StringZufriedenK2="\u2713";
				}else{
				StringZufriedenK2="\u2716"; 
				}
				
			}
		}

	}

	public double zufallGeneratorAnkunft(int erwartung) {
		double zeit = 0.0;
		while (zeit <= 0.0) {
			zeit = erwartung + (rand.nextGaussian() * 10);
		}
		return Math.rint(zeit * 100) / 100;
	}

	public double zufallGeneratorEnde(int erwartung) {
		double zeit = 0.0;
		while (zeit <= 0.0) {
			zeit = erwartung + (rand.nextGaussian() * 5);
		}
		return Math.rint(zeit * 100) / 100;
	}

	public void ausgabeGUI(JTable table) {

		if(zufriedenKoch2<=15.00){
			StringZufriedenK2="\u2713";
			}else{
			StringZufriedenK2="\u2716"; 
			}
		if(zufriedenKoch1<=15.00){
			StringZufriedenK1="\u2713";
			}else{
			StringZufriedenK1="\u2716"; 
			}
		
		table.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (value.toString().equals("a")) {
					setBackground(Color.RED);
					setHorizontalAlignment(CENTER);
					setText("a");
				} else {
					setBackground(Color.GREEN);
					setHorizontalAlignment(CENTER);
					setText("f");
				}
				return this;
			}
		});
		table.getColumnModel().getColumn(6).setCellRenderer(new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (value.toString().equals("a")) {
					setBackground(Color.RED);
					setHorizontalAlignment(CENTER);
					setText("a");
				} else {
					setBackground(Color.GREEN);
					setHorizontalAlignment(CENTER);
					setText("f");
				}
				return this;
			}
		});
		

		if (koch1 == 'a' && koch2 == 'f') {
			if(flag_zufriedenheit_1==true){
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100,
					StringEscapeUtils.unescapeJava(StringZufriedenK1), koch1, Math.rint(b2 * 100) / 100, "", koch2, warteschlange,
					Math.rint(simzeit * 100) / 100 });
			flag_zufriedenheit_1=false;
			}else{
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100,
						"", koch1, Math.rint(b2 * 100) / 100, "", koch2, warteschlange,
						Math.rint(simzeit * 100) / 100 });
			}
		} else {
			if (koch1 == 'f' && koch2 == 'a') {
				if(flag_zufriedenheit_2==true){
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100, "",
						koch1, Math.rint(b2 * 100) / 100, StringEscapeUtils.unescapeJava(StringZufriedenK2), koch2, warteschlange,
						Math.rint(simzeit * 100) / 100 });
				flag_zufriedenheit_2=false;
				}else{
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100, "",
							koch1, Math.rint(b2 * 100) / 100, "", koch2, warteschlange,
							Math.rint(simzeit * 100) / 100 });
				}
			} else {
				if (koch1 == 'a' && koch2 == 'a') {
					if(flag_zufriedenheit_1==true && flag_zufriedenheit_2==true){
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100,
							StringEscapeUtils.unescapeJava(StringZufriedenK1), koch1, Math.rint(b2 * 100) / 100,
							StringEscapeUtils.unescapeJava(StringZufriedenK2), koch2, warteschlange,
							Math.rint(simzeit * 100) / 100 });
					flag_zufriedenheit_1=false;
					flag_zufriedenheit_2=false;
					}else{
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100,
								"", koch1, Math.rint(b2 * 100) / 100,
								"", koch2, warteschlange,
								Math.rint(simzeit * 100) / 100 });
					}
				} else {
					if (koch1 == 'f' && koch2 == 'f') {
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(b1 * 100) / 100,
								"", koch1, Math.rint(b2 * 100) / 100, "", koch2, warteschlange,
								Math.rint(simzeit * 100) / 100 });
					}
				}
			}
		}
	}

	public void csvDatei(BufferedWriter schreibBuffer) throws IOException {
		count++;
		double ankunft = (Math.rint(ankunftEreignis * 100) / 100);
		double bedinStation1 = (Math.rint(b1 * 100) / 100);
		String koch_1 = Character.toString(koch1);
		String koch_2 = Character.toString(koch2);
		double bedinStation2 = (Math.rint(b2 * 100) / 100);
		double sSeit = (Math.rint(simzeit * 100) / 100);
		String zufriedenKoch_1 = StringEscapeUtils.unescapeJava(StringZufriedenK1);
		String zufriedenKoch_2 = StringEscapeUtils.unescapeJava(StringZufriedenK2);

		String zeile = String.format("%f;%f;%s;%s;%f;%s;%s;%d;%f%n",
				ankunft, 			// double
				bedinStation1, 		// double
				zufriedenKoch_1, 	// String
				koch_1,				// char
				bedinStation2,		// double
				zufriedenKoch_2, 	// String
				koch_2, 			// char
				warteschlange, 		// double
				sSeit 				// double
		);
		schreibBuffer.write(zeile);

	}
	public void csvFormeln(BufferedWriter schreibBuffer) throws IOException{
		String f = "\"f";
		String endung = ";\""+f+"\"\")";
		String max = "=MAX(H1:H"+count+")";
		String z�hlen ="\"=Z�HLENWENN(D1:D"+count;
		String z�hlen2 ="\"=Z�HLENWENN(G1:G"+count;
				
		String formeln = String.format("%n%s;%s;%s;%s;%s;%s;%s;%s;%s%n","","","",z�hlen+endung,"","",z�hlen2+endung,max,"");
				
		schreibBuffer.write(formeln);
		
		
	}

}
