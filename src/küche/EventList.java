package k�che;

import java.util.LinkedList;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

public class EventList extends LinkedList  {
	    
	
	    public EventList() {
	        super();
	    }
	    
	    public Object getMin() {        
	    	return getFirst();        
	    }
	        
	    public void enqueue(Object _o) {
	        add( _o);
	    }
	    
	    public void dequeue() {
	        removeFirst();
	    }
	
}
