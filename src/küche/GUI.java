package k�che;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class GUI implements ActionListener {

	private JFrame frame;
	private JTable table;
	private JButton btnStart = new JButton("Start");
	private JTextField txtErwAn;
	private JTextField txtErwBear;
    @SuppressWarnings("rawtypes")
	private JComboBox comboBox = new JComboBox();
	/**
	 * Launch the application.
	 */
	public void startGUI() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 869, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		btnStart.addActionListener(this);
		
		txtErwAn = new JTextField();
		txtErwAn.setColumns(10);
		
		JLabel lblErwartungswertAnkunft = new JLabel("Erwartungswert Ankunft");
		
		JLabel lblErwartungswertBearbeitung = new JLabel("Erwartungswert Bearbeitung");
		
		txtErwBear = new JTextField();
		txtErwBear.setColumns(10);
		
		JLabel lblLngeArbeitstag = new JLabel("L\u00E4nge Arbeitstag");
		
		 comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"}));

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 696, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(txtErwBear, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblErwartungswertBearbeitung))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblErwartungswertAnkunft)
								.addComponent(txtErwAn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblLngeArbeitstag, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(comboBox, Alignment.TRAILING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(186)
							.addComponent(btnStart)))
					.addContainerGap(137, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblErwartungswertAnkunft)
						.addComponent(lblLngeArbeitstag)
						.addComponent(lblErwartungswertBearbeitung))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnStart)
						.addComponent(txtErwAn, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtErwBear, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
					.addGap(10))
		);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Ankunft", "<html>Bedinungsende<br>Koch_1</html>","<html>Zufriedenheit<br>Koch_1</html>", "Koch_1",
						"<html>Bedinungsende<br>Koch_2</html>","<html>Zufriedenheit<br>Koch_2</html>", "Koch_2", "Warteschlange", 
						"<html>Simulations<br>Zeit</html>" }) {
			
							private static final long serialVersionUID = 1L;
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] { Double.class, Double.class,Object.class, Object.class, Double.class,Object.class, Object.class,
					Integer.class, Double.class };

			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		DefaultTableCellRenderer r = new DefaultTableCellRenderer() {
			
			private static final long serialVersionUID = 1L;

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				setHorizontalAlignment(JLabel.CENTER);
				return this;
			}
		};
		for (int i = 0; i < 8; i++) {
			table.getColumnModel().getColumn(i).setCellRenderer(r);
		}
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(0).setMaxWidth(50);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(92);
		table.getColumnModel().getColumn(1).setMinWidth(92);
		table.getColumnModel().getColumn(1).setMaxWidth(92);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(92);
		table.getColumnModel().getColumn(2).setMinWidth(92);
		table.getColumnModel().getColumn(2).setMaxWidth(92);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setMinWidth(50);
		table.getColumnModel().getColumn(3).setMaxWidth(50);
		table.getColumnModel().getColumn(4).setResizable(false);
		table.getColumnModel().getColumn(4).setPreferredWidth(92);
		table.getColumnModel().getColumn(4).setMinWidth(92);
		table.getColumnModel().getColumn(4).setMaxWidth(92);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(5).setPreferredWidth(92);
		table.getColumnModel().getColumn(5).setMinWidth(92);
		table.getColumnModel().getColumn(5).setMaxWidth(92);
		table.getColumnModel().getColumn(6).setResizable(false);
		table.getColumnModel().getColumn(6).setPreferredWidth(50);
		table.getColumnModel().getColumn(6).setMinWidth(50);
		table.getColumnModel().getColumn(6).setMaxWidth(50);
		table.getColumnModel().getColumn(7).setResizable(false);
		table.getColumnModel().getColumn(7).setPreferredWidth(90);
		table.getColumnModel().getColumn(7).setMinWidth(90);
		table.getColumnModel().getColumn(7).setMaxWidth(90);
		table.getColumnModel().getColumn(8).setResizable(false);
		table.getColumnModel().getColumn(8).setPreferredWidth(70);
		table.getColumnModel().getColumn(8).setMinWidth(70);
		table.getColumnModel().getColumn(8).setMaxWidth(70);
		table.getTableHeader().setPreferredSize(new Dimension(scrollPane.getWidth(), 40));
		scrollPane.setViewportView(table);
		frame.getContentPane().setLayout(groupLayout);

	}

	public void actionPerformed(ActionEvent evt) {

		Object src = evt.getSource();

		if (src == btnStart) {
			SimulationNeu simNeu = new SimulationNeu();
			
				DefaultTableModel dm = (DefaultTableModel) table.getModel();
				int rowCount = dm.getRowCount();
				if (rowCount > 0) {
					for (int i = rowCount - 1; i >= 0; i--) {
						dm.removeRow(i);
					}
				}
				
				int stundenEingelesen=  (comboBox.getSelectedIndex())+1;
				
				//double tage = Double.parseDouble(tFTage.getText());

				int erwartungAnk=Integer.parseInt(txtErwAn.getText());
				int erwartungBea=Integer.parseInt(txtErwBear.getText());
				
				double stunden =stundenEingelesen*60;

				
				simNeu.Simulation(table, stunden, erwartungAnk, erwartungBea);
				
//				if(erwartungAnk==0 && erwartungBea==0){
//			} else {
//				JOptionPane.showMessageDialog(frame, "<html>Simulationszeit in Tage<br>   darf nicht leer sein</html>");
//			}
		}

	}
	}
	

