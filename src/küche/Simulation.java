package k�che;

import java.awt.Color;
import java.awt.Component;
import java.util.*;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Simulation {

	private double simulationszeit = 0.0;
	private double simulationsende = 200.0;
	private double ankunftEreignis;
	private double koch_bedi_ende1;
	private double koch_bedi_ende2;
	private char koch1_flag;
	private char koch2_flag;
	private Bestellung bestellung;
	private int laengeWarteschlange;
	double test;
	private int erwartungBea;
	private int erwartungAnk;
	public EventList futureEventList = new EventList();

	private Random rand = new Random();

	private SortedMap<Double, Bestellung> warteschlange = new TreeMap<>();

	public void initialisierung() {
		this.koch_bedi_ende1 = 0.0;
		this.koch_bedi_ende2 = 0.0;
		this.koch1_flag = 'f';
		this.koch2_flag = 'f';
		this.ankunftEreignis = zufallsAnkunftsZeit(erwartungAnk);
		this.laengeWarteschlange = 0;
		this.test = 0.0;
		
	}

	public void simulationsmethode(JTable table, double simende,int erwartungAnk,int erwartungBea) {
		this.simulationsende = simende;
		this.erwartungAnk=erwartungAnk;
		this.erwartungBea=erwartungBea;
		initialisierung();

		while (simulationszeit < simulationsende) {

			if (koch_bedi_ende1 == 0.0 && koch_bedi_ende2 == 0.0) {
				ankunft();
			} else if (koch_bedi_ende1 == 0.0) {
				if (ankunftEreignis < koch_bedi_ende2) {
					ankunft();
				} else
					bedinungsEnde("k2");
			} else if (koch_bedi_ende2 == 0.0) {
				if (ankunftEreignis < koch_bedi_ende1) {
					ankunft();
				} else {
					bedinungsEnde("k1");
				}
			} else if (ankunftEreignis < koch_bedi_ende1 && ankunftEreignis < koch_bedi_ende2) {
				ankunft();
			} else if (koch_bedi_ende1 < koch_bedi_ende2) {
				bedinungsEnde("k1");
			} else if (koch_bedi_ende2 < koch_bedi_ende1) {
				bedinungsEnde("k2");
			} else if (koch_bedi_ende1 == koch_bedi_ende2) {
				System.out.println("test");
			}
			ausgabeGUI(table);
		}

	}

	public void ankunft() {
		
		// 1 = ankunftsereigniss 2 = ende Ereignis
		Bestellung evt = new Bestellung(ankunftEreignis,1);
		futureEventList.enqueue(evt);	
		
		bedinungsEndeNeu();
		
		simulationszeit = ankunftEreignis;
		
		//bestellung = new Bestellung(ankunftEreignis,1);
		//event.add(bestellung);
		warteschlange.put(simulationszeit, bestellung);
		laengeWarteschlange++;
		ankunftEreignis = simulationszeit + zufallsAnkunftsZeit(erwartungAnk);
		if (koch1_flag == 'f') {
			bearbeitung("k1");
		} else {
			if (koch2_flag == 'f') {
				bearbeitung("k2");
			}
		}
	}

	public void bedinungsEndeNeu(){
		
	}
	
	
	
	
	
	public void bearbeitung(String koch) {

		if (koch.equals("k1")) {
			koch1_flag = 'b';
			koch_bedi_ende1 = simulationszeit + bearbeitungsZeit(erwartungBea);

		} else if (koch.equals("k2")) {
			koch2_flag = 'b';
			koch_bedi_ende2 = simulationszeit + bearbeitungsZeit(erwartungBea);

		}
	}


	
	
	
	
	public void bedinungsEnde(String koch) {
	
		if (koch.equals("k1")) {
			koch1_flag = 'f';
			simulationszeit = koch_bedi_ende1;
			koch_bedi_ende1 = 0.0;
			
			laengeWarteschlange--;
			warteschlange.remove(warteschlange.firstKey());

		}

		if (koch.equals("k2")) {
			koch2_flag = 'f';
			simulationszeit = koch_bedi_ende2;
			koch_bedi_ende2 = 0.0;
			laengeWarteschlange--;
			warteschlange.remove(warteschlange.firstKey());

		}
	}

	public double zufallsAnkunftsZeit(int erwartung) {
		double zeit = 0.0;

		zeit = erwartung + (rand.nextGaussian() * 5);

		return Math.rint(zeit * 100) / 100;
	}

	public double bearbeitungsZeit(int erwartung) {
		double zeit = 0.0;

		zeit = erwartung + (rand.nextGaussian() * 5);

		return Math.rint(zeit * 100) / 100;
	}

	public void ausgabeGUI(JTable table) {

		table.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (value.toString().equals("b")) {
					setBackground(Color.RED);
					setHorizontalAlignment(CENTER);
					setText("b");
				} else {
					setBackground(Color.GREEN);
					setHorizontalAlignment(CENTER);
					setText("f");
				}
				return this;
			}
		});
		table.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (value.toString().equals("b")) {
					setBackground(Color.RED);
					setHorizontalAlignment(CENTER);
					setText("b");
				} else {
					setBackground(Color.GREEN);
					setHorizontalAlignment(CENTER);
					setText("f");
				}
				return this;
			}
		});

		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.addRow(new Object[] { Math.rint(ankunftEreignis * 100) / 100, Math.rint(koch_bedi_ende1 * 100) / 100,
				koch1_flag, Math.rint(koch_bedi_ende2 * 100) / 100, koch2_flag, laengeWarteschlange, "test",
				Math.rint(simulationszeit * 100) / 100 });

	}

	public double getSimulationsende() {
		return simulationsende;
	}

	public void setSimulationsende(double simulationsende) {
		this.simulationsende = simulationsende;
	}
}
